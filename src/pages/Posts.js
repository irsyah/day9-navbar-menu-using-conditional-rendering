import React, { useState, useEffect } from 'react';
import FormPost from '../components/FormPost';
import Post from '../components/Post';


function Posts() {
  const [ posts, setPosts ] = useState([]);
  const [ isAdd, setIsAdd ] = useState(true);
  const [ newUpdatedPost, setNewUpdatedPost ] = useState({});

  const getPosts = async () => {
    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts`);
        if (response.ok) {
            const data = await response.json();
            setPosts(data);
        } else {
            throw Error('something wrong');
        }
    } catch(err) {
        console.log(err);
    }
  }

  const formEditPost = (updatePost) => {
    setIsAdd(false);
    setNewUpdatedPost(updatePost)
  }

  const resetFormFromParents = () => {
    setIsAdd(true);
    setNewUpdatedPost({});
  }

  useEffect(() => {
    getPosts();
  }, []);

  return (
    <div>
        <FormPost 
            isAdd={isAdd} 
            setPosts={setPosts} 
            newUpdatedPost={newUpdatedPost} 
            resetFormFromParents={resetFormFromParents}
        />
        { posts.map(post => (
            <Post 
                key={post.id} 
                post={post} 
                setPosts={setPosts} 
                formEditPost={formEditPost}
            />
        ))}
    </div>
  )
}

export default Posts
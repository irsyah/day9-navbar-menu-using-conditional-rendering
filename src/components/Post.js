import React from 'react';
import { Card, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

function Post(props) {
  const { post, setPosts, formEditPost } = props;

  const removePost = () => {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(async (result) => {
        try {
            const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${post.id}`, {
                method: 'DELETE'
            })
    
            if (response.ok) {
                // baru ubah data state
                setPosts((currentState) => {
                     return currentState.filter(state => {
                        if (state.id !== post.id) {
                            return state;
                        }
                    })
                });
                if (result.isConfirmed) {
                    Swal.fire(
                      'Deleted!',
                      'Your file has been deleted.',
                      'success'
                    )
                  }
            } else {
                throw Error('something wrong');
            }
        } catch(err) {
          console.log(err);
        }
      })
  }

  const editPost = () => {
    formEditPost(post);
  }

  return (
    <Card style={{ width: '18rem' }}>
        <Card.Body>
        <Card.Title>{post.title}</Card.Title>
        <Card.Text>
            {post.body}
        </Card.Text>
        <Button onClick={removePost} variant="danger">Delete</Button>
        <Button onClick={editPost} variant="primary">Edit</Button>
        </Card.Body>
    </Card>
  )
}

export default Post
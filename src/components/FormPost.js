import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { Form, Button } from 'react-bootstrap';

function FormPost({ isAdd, setPosts, newUpdatedPost, resetFormFromParents }) {

    const [ post, setPost ] = useState({
        userId: '',
        title: '',
        body: ''
    });

    useEffect(() => {
        setPost(newUpdatedPost)
    }, [ newUpdatedPost ])

    const onChangePost = (e) => {
        setPost(currState => {
            return { ...currState, [e.target.id]: e.target.value }
        })
    } 

    const save = async (e) => {
        e.preventDefault();
        
        if (isAdd) {
            // SAVE
            try {
                const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
                                                method: 'POST',
                                                body: JSON.stringify(post),
                                                headers: {
                                                    'Content-type': 'application/json; charset=UTF-8',
                                                },
                                            })
                if (response.ok) {
                    // mau ubah data parents
                    const data = await response.json();

                    setPosts(currState => {
                        return [ ...currState, data ]
                    })

                    resetForm();
                } else {
                    throw Error('something wrong');
                }
                
            } catch (err) {
                console.log(err);
            }
        } else {
            // EDIT
            try {
                const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${post.id}`, {
                                                method: 'PUT',
                                                body: JSON.stringify(post),
                                                headers: {
                                                    'Content-type': 'application/json; charset=UTF-8',
                                                },
                                            })
                if (response.ok) {
                    // mau ubah data parents
                    const data = await response.json();

                    setPosts(currState => {
                        return currState.map(state => {
                            if (state.id === post.id) {
                                state = data;
                            }
                            return state;
                        })
                    })

                    // kembalikan ke default form Add
                    resetFormFromParents();
                    resetForm()

                } else {
                    throw Error('something wrong');
                }
                
            } catch (err) {
                console.log(err);
            }
        }
    }

    const resetForm = () => {
        setPost({
            userId: '',
            title: '',
            body: ''
        })
    }

    return (
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>UserId</Form.Label>
            <Form.Control 
                id="userId" 
                type="text" 
                placeholder="Enter userId" 
                onChange={onChangePost}
                value={post.userId}
            />
          </Form.Group>
    
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Title</Form.Label>
            <Form.Control 
                id="title" 
                type="text" 
                placeholder="Enter Title" 
                onChange={onChangePost}
                value={post.title}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Body</Form.Label>
            <Form.Control 
                id="body" 
                type="text" 
                placeholder="Enter Body" 
                onChange={onChangePost}
                value={post.body}
            />
          </Form.Group>

          <Button variant="primary" type="submit" onClick={save}>
            Submit
          </Button>
        </Form>
    );
}

export default FormPost
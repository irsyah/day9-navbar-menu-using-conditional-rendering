import { useState } from 'react';
import Swal from 'sweetalert2';
import { Button, Card } from 'react-bootstrap';

import MyNavbar from "./components/MyNavbar";
import Home from './pages/Home';
import Posts from './pages/Posts';

function App() {

  const [ menu, setMenu ] = useState('Home');

  return (
    <div >
      <MyNavbar setMenu={setMenu}/>

      { menu === 'Home' ? <Home /> : (menu === 'Post') ? <Posts /> : <Home />}



      {/* <button onClick={click} type="button" className="btn btn-primary">Click Me</button>
      <Button onClick={click} variant='primary'>Save</Button>

      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src="holder.js/100px180" />
        <Card.Body>
          <Card.Title>Card Title</Card.Title>
          <Card.Text>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card> */}
    </div>
  );
}


const click = () => {
  // Swal.fire({
  //   title: 'Error!',
  //   text: 'Do you want to continue',
  //   icon: 'error',
  //   confirmButtonText: 'Cool'
  // })

  Swal.fire({
    title: 'Do you want to save the changes?',
    showDenyButton: true,
    showCancelButton: false,
    confirmButtonText: 'Save',
    denyButtonText: `Don't save`,
  }).then((result) => {
    /* Read more about isConfirmed, isDenied below */
    if (result.isConfirmed) {
      Swal.fire('Saved!', '', 'success')
    } else if (result.isDenied) {
      Swal.fire('Changes are not saved', '', 'info')
    }
  })
}

export default App;
